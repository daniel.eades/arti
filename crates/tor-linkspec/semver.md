BREAKING: Rename UnparsedLinkSpec => EncodedLinkSpec
BREAKING: EncodedLinkSpec::new() now takes LinkSpecType.
ADDED: LinkSpecType as a public type.

